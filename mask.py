 
#Importing libraries
import numpy as np
import scipy.misc as imsave
import matplotlib.pyplot as plt
import cmath as cm

'''
###### Phase of a complex Image #####
N: matrix size
Ima:  Complex Image
Fase: Phase of the complex image
'''
def FaseIma(N,Ima): 
    Fase = np.zeros((N,N))
    for cont1 in np.arange(0,N):
        for cont2 in np.arange(0,N):
            Fase[cont1,cont2] = cm.phase(Ima[cont1,cont2])
    return Fase


#%% Parameters
dx = 0.02       # Step or milimeters resolution
N = 800         # Size on pixels
n = 2           # Level Phase or discretization phase
m = 1.0         # Topological charge
k = 4           # Wave number (Units??)

#%% Coordinate workspace creating
x = np.arange(-N*dx/2, N*dx/2, dx)  # x coordinate
X, Y = np.meshgrid(x, x)            # 2D coordinate space

#%% Spiral Phase Mask
theta = np.arctan2(Y, X); # Angle phase
SPM = np.exp(1j*m*theta)

#%% Holographic mask
HM = 2*(1+np.cos(2*np.pi*X/k-m*theta)); #From 2008_AJP_Carpentier_v76_n10_p916



#%% Plotting
plt.figure(1)
#plt.imshow(FaseIma(N,SPM),cmap='gray',interpolation='Nearest')
plt.imshow(FaseIma(N,HM),cmap='gray',interpolation='Nearest')
plt.show

#%% Discretization
#argu = np.floor(n*(np.pi + FaseIma(N,SPM))/(2.*np.pi))/n
argu = np.floor(n*(np.pi + FaseIma(N,HM))/(2.*np.pi))/n
arguM = 2.*np.pi*argu - np.pi

#SPM_D = np.exp(1j*arguM)
HM_D = 2*(1+np.cos(2*np.pi*X/k-arguM))
plt.figure(2)
#plt.imshow(FaseIma(N,SPM_D),cmap='gray',interpolation='Nearest')
plt.imshow(FaseIma(N,HM_D),cmap='gray',interpolation='Nearest')
#plt.colorbar()
plt.show

#%%
imsave('mask.png', arguM)










