# -*- coding: utf-8 -*-
"""
Created on Wed Oct  5 16:15:59 2016

@author: ER
"""

# Librerías
import numpy as np
import pylab as plt
from scipy.misc import imsave
import cmath as cm

'''
###### Phase of a complex Image #####
N: matrix size
Ima:  Complex Image
Fase: Phase of the complex image
'''
def FaseIma(N,Ima): 
    Fase = np.zeros((N,N))
    for cont1 in np.arange(0,N):
        for cont2 in np.arange(0,N):
            Fase[cont1,cont2] = cm.phase(Ima[cont1,cont2])
    return Fase


#%% Parámetros
N = 800 # tamaño en pixeles
dx = 0.02 # resolución en milímetros
print('Resolución de %.3f mm' % dx)
print('Lineas por mm = %.2f' % (1/dx))

m = 1.0 #carga topológica
M = 3 # niveles de discretización


#%% Creación del espacio coordenado
x = np.arange(-N*dx/2,N*dx/2,dx) # coordenada x
X , Y = np.meshgrid(x,x) # espacio coord 2D

#%% Máscara de fase espiral (SPP)
theta = np.arctan2(Y,X) # ángulo azimutal

SPP = np.exp(1j*m*theta)

plt.figure(1)
plt.imshow(FaseIma(N,SPP),cmap='gray',interpolation='Nearest')
#plt.colorbar()
plt.show

#%% Discretización
argu = np.floor(M*(np.pi + FaseIma(N,SPP))/(2.*np.pi))/M

arguM = 2.*np.pi*argu - np.pi
SPPM = np.exp(1j*arguM)

plt.figure(2)
plt.imshow(FaseIma(N,SPPM),cmap='gray',interpolation='Nearest')
#plt.colorbar()
plt.show

#%%
imsave('mascara.png',arguM)


