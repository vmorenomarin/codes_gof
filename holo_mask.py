 
#Importing libraries
import numpy as np
import matplotlib.pyplot as plt
from math import *


# Image resolution
x_r = 800 # Valor en x
y_r = 600 # Valor en y

x1 = np.array([range(1,x_r+1)])
y1 = np.array([range(1,y_r+1)])

X, Y = np.meshgrid(x1, y1)


#Physics parameters

k = 4 # Wave number (Units??)
m = 1 # Topologycal charge
theta = np.arctan2((Y - y_r/2),(X - x_r/2)); # Angle phase

#Holographic Mask
H=2*(1+np.cos(2*pi*X/k-m*theta)); #From 2008_AJP_Carpentier_v76_n10_p916


#Plotting 
plt.plot(H)
plt.imshow(H, cmap='Greys')
plt.show()







